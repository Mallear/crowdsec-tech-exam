locals {
  aws_account_id = "${get_env("aws_account_id", "")}"
  project  = "crowdsec-honeypot"
}
