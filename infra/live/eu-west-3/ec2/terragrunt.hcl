locals {
  # Automatically load project-level variables
  common_vars = read_terragrunt_config(find_in_parent_folders("common.hcl"))
  project     = local.common_vars.locals.project

  name_suffix = "${local.project}"

  tags = {
    Project     = local.project
    Deployer    = "terraform"
  }
}


# Terragrunt will copy the Terraform configurations specified by the source parameter, along with any files in the
# working directory, into a temporary folder, and execute your Terraform commands in that folder.
terraform {
  # source = "github.com/terraform-aws-modules/terraform-aws-ec2-instance.git//?ref=${local.module_version}"
  source = "../../../modules/terraform-module-aws-ec2"
}

# Include all settings from the root terragrunt.hcl file
include {
  path = find_in_parent_folders()
}

dependency "vpc" {
  config_path = "../vpc"
}
dependency "bastion" {
  config_path = "../bastion"
}

dependencies {
  paths = ["../vpc", "../bastion"]
}


inputs = {

  instance_name = "honey-pot"
  ssh_key_name  = "crowdsec"
  name_suffix  = local.name_suffix

  instance_count = 1

  ami           = "ami-04e905a52ec8010b2" # Debian 10
  instance_type = "t2.small"
  subnet_id     = dependency.vpc.outputs.public_subnets
  associate_public_ip_address = true

  vpc_id        = dependency.vpc.outputs.vpc_id
  vpc_subnet_id = dependency.vpc.outputs.public_subnets[0]


  root_block_device = [
    {
      volume_type = "gp2"
      volume_size = 10
      tags = local.tags
    },
  ]

  ingress_cidr = [
    {
      port = "80"
      protocol = "TCP"
      allowed_cidr = ["0.0.0.0/0"]
    }
  ]
  ingress_security_groups = [
    {
      port = "22"
      protocol = "TCP"
      allowed_security_groups = [dependency.bastion.outputs.security_group_id]
    }
  ]

  tags = local.tags
}
