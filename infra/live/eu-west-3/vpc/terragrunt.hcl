locals {
  # Automatically load project-level variables
  common_vars = read_terragrunt_config(find_in_parent_folders("common.hcl"))
  project     = local.common_vars.locals.project

  module_version = "v3.2.0"

  name_suffix = "${local.project}"

  tags = {
    Project     = local.project
    Deployer    = "terraform"
  }
}


# Terragrunt will copy the Terraform configurations specified by the source parameter, along with any files in the
# working directory, into a temporary folder, and execute your Terraform commands in that folder.
terraform {
  source = "github.com/terraform-aws-modules/terraform-aws-vpc.git//?ref=${local.module_version}"
}

# Include all settings from the root terragrunt.hcl file
include {
  path = find_in_parent_folders()
}

inputs = {

  name = "vpc-${local.name_suffix}"

  cidr = "10.10.0.0/16"

  azs                 = ["eu-west-3a","eu-west-3b"]
  public_subnets      = ["10.10.8.0/21", "10.10.24.0/21"]
  private_subnets     = ["10.10.40.0/21", "10.10.56.0/21"]

  enable_dns_hostnames = true
  enable_dns_support   = true

  enable_nat_gateway = true

  # Default security group - ingress/egress rules cleared to deny all
  manage_default_security_group  = true
  default_security_group_ingress = []
  default_security_group_egress  = []

  tags = local.tags
}
