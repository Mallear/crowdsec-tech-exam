locals {
  # Automatically load project-level variables
  common_vars = read_terragrunt_config(find_in_parent_folders("common.hcl"))
  project     = local.common_vars.locals.project

  name_suffix = "${local.project}"

  tags = {
    Project     = local.project
    Deployer    = "terraform"
  }
}


# Terragrunt will copy the Terraform configurations specified by the source parameter, along with any files in the
# working directory, into a temporary folder, and execute your Terraform commands in that folder.
terraform {
  # source = "github.com/terraform-aws-modules/terraform-aws-ec2-instance.git//?ref=${local.module_version}"
  source = "../../../modules/terraform-module-aws-alb"
}

# Include all settings from the root terragrunt.hcl file
include {
  path = find_in_parent_folders()
}

dependency "vpc" {
  config_path = "../vpc"
}
dependency "ec2" {
  config_path = "../ec2"
}

dependencies {
  paths = ["../vpc", "../ec2"]
}


inputs = {

  name = "honey-pot-lb"
  name_suffix  = local.name_suffix

  load_balancer_type = "application"

  vpc_id             = dependency.vpc.outputs.vpc_id
  subnets            = dependency.vpc.outputs.public_subnets


  target_groups = [
    {
      name_prefix      = "tg-"
      backend_protocol = "HTTP"
      backend_port     = 80
      target_type      = "instance"
      targets = [
        {
          target_id = dependency.ec2.outputs.ec2_ids[0]
          port = 80
        }
      ]
    }
  ]

  http_tcp_listeners = [
    {
      port               = 80
      protocol           = "HTTP"
      target_group_index = 0
    }
  ]

  ingress_cidr = [
    {
      port = "80"
      protocol = "TCP"
      allowed_cidr = ["0.0.0.0/0"]
    }
  ]

  tags = local.tags
}
