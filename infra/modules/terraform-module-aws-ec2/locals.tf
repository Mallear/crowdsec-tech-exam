locals {
  ingress_security_groups = length(var.ingress_security_groups) < 1 ? [] : flatten([
    for item in var.ingress_security_groups : [
      for security_group_id in item.allowed_security_groups : [{
        from_port               = length(split(":", item.port)) > 1 ? tonumber(element(split(":", item.port), 0)) : tonumber(item.port)
        to_port                 = length(split(":", item.port)) > 1 ? tonumber(element(split(":", item.port), 1)) : tonumber(item.port)
        protocol                = item.protocol
        allowed_security_groups = security_group_id
      }]
    ]
  ])
  ingress_cidr = length(var.ingress_cidr) < 1 ? [] : flatten([
    for item in var.ingress_cidr : [{
      from_port    = length(split(":", item.port)) > 1 ? tonumber(element(split(":", item.port), 0)) : tonumber(item.port)
      to_port      = length(split(":", item.port)) > 1 ? tonumber(element(split(":", item.port), 1)) : tonumber(item.port)
      protocol     = item.protocol
      allowed_cidr = item.allowed_cidr
    }]
  ])

  root_block_device = var.root_block_device == [] ? [] : [
    for value in var.root_block_device:
      merge(value, { tags = var.tags })
    ]
}
