output "security_group_id" {
  description = "Instance security group ID"
  value       = aws_security_group.security_group.id
}

output "public_ip_address" {
  description = "Instance public IP address if enabled"
  value       = var.associate_public_ip_address ? module.ec2.public_ip : [""]
}

output "ec2_ids" {
  description = "Instance ids"
  value       = module.ec2.id
}

output "ec2_arns" {
  description = "Instance arns"
  value       = module.ec2.arn
}

output "instance_dns_record_name" {
  description = "Instance record name"
  value       = (var.associate_public_ip_address && length(var.zone_name) > 0) ? aws_route53_record.record[0].fqdn : ""
}
