module "ec2" {
  source         = "terraform-aws-modules/ec2-instance/aws"
  version        = "~> 2.19"
  instance_count = 1

  name                        = join("-", [var.instance_name, var.name_suffix])
  ami                         = var.ami
  instance_type               = var.instance_size
  subnet_id                   = var.vpc_subnet_id
  vpc_security_group_ids      = [aws_security_group.security_group.id]
  associate_public_ip_address = var.associate_public_ip_address

  key_name = var.ssh_key_name

  root_block_device = var.root_block_device

  enable_volume_tags = false

  ebs_block_device = var.ebs_block_device

  user_data            = var.user_data != "" ? var.user_data : null

  tags = var.tags
}

resource "aws_security_group" "security_group" {
  name        = join("-", [var.instance_name, var.name_suffix])
  description = "${var.instance_name} security group"
  vpc_id      = var.vpc_id

  tags = var.tags
}
resource "aws_security_group_rule" "egress_on_the_internet" {
  type              = "egress"
  from_port         = 0
  to_port           = 65535
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.security_group.id
}
