variable "name_suffix" {
  description = "Suffix to append to resources name"
  type        = string
}

variable "instance_name" {
  type        = string
  description = "Instance name"
}

variable "instance_size" {
  type        = string
  description = "Instance size"
  default     = "t3.small"
}

variable "ami" {
  type        = string
  description = "AWS image AMI"
}

variable "user_data" {
  type        = string
  description = "User data to run at the instance boot"
  default     = ""
}

variable "ssh_key_name" {
  type        = string
  description = "SSH key name to setup on the instance"
}


# Network
variable "vpc_id" {
  type        = string
  description = "VPC ID"
}
variable "vpc_subnet_id" {
  type        = string
  description = "VPC Subnet to creation the instance in"
}
variable "associate_public_ip_address" {
  type        = bool
  description = "Wether to create public IP address for the instance"
  default     = false
}

variable "ingress_security_groups" {
  type = list(object({
    port                    = string
    protocol                = string
    allowed_security_groups = list(string)
  }))
  default     = []
  description = "List of Security Group IDs that are allowed ingress to the instance's Security Group created in the module"
}

variable "ingress_cidr" {
  type = list(object({
    port         = string
    protocol     = string
    allowed_cidr = list(string)
  }))
  default     = []
  description = "List of cidr that are allowed ingress to the instance's Security Group created in the module"
}

# Block storage
variable "root_block_device" {
  type        = list(object({
      volume_type = string
      volume_size = number
      tags        = map(any)
  }))
  description = "Root block storage type"
  default     = []
}

variable "ebs_block_device" {
  type        = list(map(any))
  description = "EBS block storage definition"
  default     = []
}

variable "tags" {
  description = "List of tags to apply to resources"
  type        = map(string)
}

### Route 53
variable "zone_id" {
  type        = string
  default     = ""
  description = "Route53 DNS Zone ID. Specify zone_name or zone_id."
}

variable "zone_name" {
  type        = string
  default     = ""
  description = "Route53 DNS Zone name. Specify zone_name or zone_id."
}

variable "dns_subdomain" {
  type        = string
  default     = ""
  description = "The subdomain to use for the CNAME record. If not provided then the CNAME record will use var.name."
}

variable "ttl" {
  type        = number
  default     = 60
  description = "Route53 record TTL"
}
