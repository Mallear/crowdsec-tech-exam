data "aws_route53_zone" "zone" {
  count = length(var.zone_name) > 0 ? 1 : 0
  name  = var.zone_name
}

resource "aws_route53_record" "record" {
  count   = (var.associate_public_ip_address && length(var.zone_name) > 0) ? 1 : 0
  zone_id = var.zone_name != "" ? data.aws_route53_zone.zone[0].zone_id : var.zone_id
  name    = var.dns_subdomain != "" ? var.dns_subdomain : join("-", [var.instance_name, var.name_suffix])
  type    = "CNAME"
  ttl     = var.ttl
  records = module.ec2.public_dns
}
