
resource "aws_security_group_rule" "ingress_cidr" {
  count             = length(local.ingress_cidr)
  description       = "Allow ${lookup(local.ingress_cidr[count.index], "protocol")} inbound traffic from ${lookup(local.ingress_cidr[count.index], "from_port")} to ${lookup(local.ingress_cidr[count.index], "to_port")}"
  from_port         = lookup(local.ingress_cidr[count.index], "from_port")
  to_port           = lookup(local.ingress_cidr[count.index], "to_port")
  protocol          = lookup(local.ingress_cidr[count.index], "protocol")
  cidr_blocks       = lookup(local.ingress_cidr[count.index], "allowed_cidr")
  security_group_id = aws_security_group.security_group.id
  type              = "ingress"
}
