module "alb" {
  source         = "terraform-aws-modules/alb/aws"
  version = "~> 6.0"

  name                        = join("-", [var.name, var.name_suffix])

  load_balancer_type = var.load_balancer_type

  vpc_id             = var.vpc_id
  subnets            = var.subnets
  security_groups    = [aws_security_group.security_group.id]

  target_groups = var.target_groups

  https_listeners = var.https_listeners

  http_tcp_listeners = var.http_tcp_listeners

  tags = var.tags
}

resource "aws_security_group" "security_group" {
  name        = join("-", [var.name, var.name_suffix])
  description = "${var.name} security group"
  vpc_id      = var.vpc_id

  tags = var.tags
}
resource "aws_security_group_rule" "egress_on_the_internet" {
  type              = "egress"
  from_port         = 0
  to_port           = 65535
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.security_group.id
}
