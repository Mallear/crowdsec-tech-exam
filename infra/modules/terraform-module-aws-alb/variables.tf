
variable "name" {
  type = string
}

variable "name_suffix" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "load_balancer_type" {
  type = string
}

variable "subnets" {
  type = list(string)
}

variable "target_groups" {
  type = list(any)
}

variable "https_listeners" {
  type = list(any)
  default = []
}

variable "http_tcp_listeners" {
  type = list(any)
}

variable "tags" {
  type = map(any)
}

variable "ingress_cidr" {
  type = list(object({
    port         = string
    protocol     = string
    allowed_cidr = list(string)
  }))
  default     = []
  description = "List of cidr that are allowed ingress to the instance's Security Group created in the module"
}