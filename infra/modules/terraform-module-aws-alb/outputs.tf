
output "security_group_id" {
  description = "ALB security group ID"
  value       = aws_security_group.security_group.id
}