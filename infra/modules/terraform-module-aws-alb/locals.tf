locals {
  ingress_cidr = length(var.ingress_cidr) < 1 ? [] : flatten([
    for item in var.ingress_cidr : [{
      from_port    = length(split(":", item.port)) > 1 ? tonumber(element(split(":", item.port), 0)) : tonumber(item.port)
      to_port      = length(split(":", item.port)) > 1 ? tonumber(element(split(":", item.port), 1)) : tonumber(item.port)
      protocol     = item.protocol
      allowed_cidr = item.allowed_cidr
    }]
  ])

}
