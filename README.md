# crowdsec-honeypot
---

# Infrastructure deployment

Dependencies:
- Terragrunt
- tfenv (to manage terraform version using .terraform-version file)

```bash
$ cd infra/live/eu-west-3
$ terragrunt run-all plan
$ terragrunt run-all apply
```

# App deployment

Deploying Nginx and Ghost based on [mtpereira work](https://galaxy.ansible.com/mtpereira/ghost).

Dependencies:
- ansible-core
- ansible

Setup instance and bastion IPs in `app/inventory/inv.yaml` and `app/inventory/group_vars/behind_bastion.yaml`.

```bash
$ cd app
$ ansible-playbook ansible.yaml
```
